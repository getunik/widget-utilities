<?php

namespace RaiseNow\WidgetUtilities;

use RaiseNow\WidgetUtilities\EMailMessage;

class EMailMessageImpl implements EMailMessage
{

    private $message;

    private $mailer;

    public function __construct($message, $mailer)
    {
        $this->message = $message;
        $this->mailer = $mailer;
    }

    public function setSubject($subject)
    {
        $this->message->setSubject($subject);
    }

    public function setTo($to)
    {
        $this->message->setTo($to);
    }

    public function setBcc($bcc)
    {
        $this->message->setBcc($bcc);
    }

    public function setFrom($from)
    {
        $this->message->setFrom($from);
    }

    public function setBody($body, $contentType)
    {
        $this->message->setBody($body, $contentType);
    }

    public function send()
    {
        return $this->mailer->send($this->message);
    }
}

?>