<?php

namespace RaiseNow\WidgetUtilities;

use ArrayIterator;

class YAMLObjectIterator extends ArrayIterator
{
    public function current()
    {
        return is_array(parent::current()) ? new YAMLObject(parent::current()) : parent::current();
    }
}
