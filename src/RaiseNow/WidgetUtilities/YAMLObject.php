<?php

namespace RaiseNow\WidgetUtilities;

use ArrayAccess;
use IteratorAggregate;
use Spyc;

class YAMLObject implements ArrayAccess, IteratorAggregate {
	private $data;

	public function __construct($data) {
		$this->data = $data;
	}

	protected function loadFromFile($file) {
		$this->data = Spyc::YAMLLoad($file);
	}

	public static function FromFile($file) {
		return new YAMLObject(Spyc::YAMLLoad($file));
	}

	public function get($key, $allowNull = false) {
		$segments = explode('.', $key);
		$subTree = $this->data;

		foreach ($segments as $subKey) {
			if (array_key_exists($subKey, $subTree)) {
				$subTree = $subTree[$subKey];
			} else {
				return $allowNull ? null : $key;
			}
		}

		return is_array($subTree) ? new YAMLObject($subTree) : $subTree;
	}

	public function getValue() {
		return $this->data;
	}

	public function toJson() {
		return json_encode($this->data);
	}

	public function mergeInto(&$array) {
		foreach ($this as $key => $value) {
			$array[$key] = $value;
		}
	}

	public function __toString()
	{
		return $this->toJson();
	}

	// ArrayAccess implementation

	public function offsetSet($offset, $value) {
        throw new Exception("Operation not supported");
    }
    public function offsetExists($offset) {
		$val = $this->get($offset, true);
        return $val !== null;
    }
    public function offsetUnset($offset) {
        throw new Exception("Operation not supported");
    }
    public function offsetGet($offset) {
		$val = $this->get($offset);
        return isset($val) ? $val : null;
    }

	// IteratorAggregate implementation

	public function getIterator() {
        return new YAMLObjectIterator($this->data);
    }
}
