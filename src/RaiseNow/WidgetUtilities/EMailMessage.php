<?php

namespace RaiseNow\WidgetUtilities;

use RaiseNow\WidgetUtilities\Message;

interface EMailMessage extends Message
{

    public function setSubject($subject);

    public function setTo($to);

    public function setBcc($bcc);

    public function setFrom($from);

    public function setBody($body, $contentType);

}

?>
