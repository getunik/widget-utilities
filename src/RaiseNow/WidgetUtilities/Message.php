<?php

namespace RaiseNow\WidgetUtilities;

interface Message
{

    public function send();
}

?>
