<?php

namespace RaiseNow\WidgetUtilities;

use RaiseNow\WidgetUtilities\EPIK;

class EPIKDefaultImpl extends EPIK
{

    public function __construct()
    {

    }

    /**
     * Returns a transaction id, unique for the merchant
     *
     * @return transactionId
     */
    public function getNextTransactionID()
    {
        return time();
    }

    /**
     * Returns a reference number for a payement slip
     *
     * @return transactionId
     */
    public function getNextRefNo()
    {
        return "";
    }

}

?>
