<?php

namespace RaiseNow\WidgetUtilities;

use RaiseNow\WidgetUtilities\YAMLObject;

class Translator extends YAMLObject
{

    function __construct($path, $language)
    {
        $file = $path . "translations.$language.yaml";
        $this->loadFromFile($file);
    }

}
