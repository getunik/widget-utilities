<?php

namespace RaiseNow\WidgetUtilities;

use Swift_Message;
use Swift_SmtpTransport;
use Swift_Mailer;
use ReflectionClass;
use ReflectionProperty;
use RaiseNow\WidgetUtilities\EMailMessageImpl;

abstract class EPIK {

    private $protocol;
    private $authority;
    private $transactionStatusApi;
    private $transactionApiUsername;
    private $transactionApiPassword;
    private $finalStatusApi;
    private $addMerchantParameterApi;
    private $recurrentPaymentApi;
    private $recurrentPaymentApiUsername;
    private $recurrentPaymentPassword;
    private $merchantConfigId;
    private $parameters;
    private $cookieParameters;
    private $cookieMerchantParameters;
    private $path;

    public static $PARAM_NAME_PROTOCOL = array("protocol", "setProtocol");
    public static $PARAM_NAME_AUTHORITY = array("authority", "setAuthority");
    public static $PARAM_NAME_TRANSACTION_STATUS_API = array("transaction_api", "setTransactionStatusApi");
    public static $PARAM_NAME_API_USERNAME = array("username", array(0 => "setTransactionApiUsername", 1 => "setRecurrentPaymentApiUsername"));
    public static $PARAM_NAME_API_PASSWORD = array("password", array(0 => "setTransactionApiPassword", 1 => "setRecurrentPaymentApiPassword"));
    public static $PARAM_NAME_FINAL_STATUS_API = array("transaction_api_setfinalstatus", "setFinalStatusApi");
    public static $PARAM_NAME_ADD_MERCHANT_PARAMETER_API = array("transaction_api_addmerchantparameter", "setAddMerchantParameterApi");
    public static $PARAM_NAME_RECURRENT_PAYMENT_API = array("recurrentpayment_subscription_api", "setRecurrentPaymentApi");
    public static $PARAM_NAME_MERCHANT_CONFIG_ID = array("merchant_config_id", "setMerchantConfigId");


    /**
     * returns the next transactionID to use.
     */
    public abstract function getNextTransactionID();

    /**
     * returns the next RefNo
     */
    public abstract function getNextRefNo();

    /**
     * Loads parameters from file and tries to apply them. group name and name must not be
     * changed.
     *
     * @param $path to the parameters.ini file
     *
     */
    public function loadParameters($path) {

        $paramName2MethodName = array();

        $reflect = new ReflectionClass("RaiseNow\WidgetUtilities\EPIK");
        $props = $reflect->getProperties(ReflectionProperty::IS_STATIC && ReflectionProperty::IS_PUBLIC);

        foreach ($props as $prop) {
            $arr = $prop->getValue();
            $paramName2MethodName[$arr[0]] = $arr[1];
        }

        $this->getParameters($path);
        foreach ($this->parameters as $paramGroupName => $paramGroup) {
            foreach ($paramGroup as $key => $val) {
                if (array_key_exists($key, $paramName2MethodName)) {
                    if (is_array($paramName2MethodName[$key])) {
                        foreach ($paramName2MethodName[$key] as $method) {
                            $reflect->getMethod($method)->invoke($this, $val);
                        }
                    } else {
                        $reflect->getMethod($paramName2MethodName[$key])->invoke($this, $val);
                    }
                }
            }
        }
        return $this->parameters;
    }

    /**
     * Returns the parameters.
     *
     * @param $path to the parameters.ini file
     *
     */
    public function getParameters($path = false) {
        if ($this->path && ($this->path == $path || !$path) && $this->parameters) {
            return $this->parameters;
        }

        $this->parameters = parse_ini_file($path, true);
        $this->path = $path;
        return $this->parameters;
    }

    /**
     * Creates a JavaScript Object from the specified parameter groups as String.
     *
     * @param array $params specifies all parameters. These parameters should be loaded from parameters.ini
     * @param array $groups specifies the name of the parameter groups which should be considered. Do not add the
     * @param string $objectName defines the name of the JavaScript object.
     *
     * @return string a JavaScript object as string
     *
     */
    public function createJavascriptOptionObject($params, $groups, $objectName) {
        $apiEndPoint = array();
        $optionString = "var " . $objectName . " = new Object();";
        foreach ($groups as $group) {
            $isServerSide = $group == "serverside" ? true : false;
            foreach ($params[$group] as $key => $val) {
                if ($isServerSide) {
                    switch ($key) {
                        case "protocol":
                            $apiEndPoint[0] = $val . "://";
                            break;
                        case "authority":
                            $apiEndPoint[1] = $val;
                            break;
                        case "pay_api":
                            $apiEndPoint[2] = $val;
                            break;
                        case "merchant_config_id":
                            $apiEndPoint[3] = $val;
                            break;
                    }

                } else {
                    if ($val === "1" || $val === "") {
                        $val = $val ? "true" : "false";
                    }
                    $optionString = $optionString . $objectName . '["' . $key . '"] = "' . $val . '";';
                }
            }
            ksort($apiEndPoint);
            $optionString .= $isServerSide ? $objectName . '["apiendpoint"] = "' . implode("", $apiEndPoint) . '";' : '';
        }
        return $optionString;
    }

    /**
     * Applies the parameters from the request to a string
     *
     * @param array $request
     * @param string $body specfies a string, which contains placesholders. These placeholders have to start with
     * have to start and end with %
     *
     * @return string $body with replaced placeholders
     *
     */
    public function applyParamsToEmailBody($request, $body) {
        $parameters = array();
        foreach ($request as $key => $val) {
            if (strpos($key, "%") == 0 && strrpos($key, "%") == strlen($key) - 1) {
                $parameters[$key] = $val;
            } else {
                $parameters["%" . $key . "%"] = $val;
            }
        }

        return strtr($body, $parameters);
    }

    /**
     *    Creates an email message based on the parameters
     *
     * @param array $params
     * @return EMailMessageImpl emailmessage
     */
    public function createNewEmailMessage($params) {
        $message = Swift_Message::newInstance();

        $mailer = $this->initMailer($params);

        return new EMailMessageImpl($message, $mailer);
    }

    /**
     * Initialiazes a mailer. This method should only be used when customization
     * of the transport method, headers or mailer is needed.
     *
     * @param array $params
     * @return Swift_Mailer mailer
     */
    public function initMailer($params) {
        //using swiftmailer to send messages.
        $transport = Swift_SmtpTransport::newInstance($params["smtp_host"]);

        if (!empty($params["smtp_username"]) && !empty($params["smtp_password"])) {
            $transport->setUsername($params["smtp_username"]);
            $transport->setPassword($params["smtp_password"]);
        }
        if (!empty($params["smtp_encription"])) {
            $transport->setEncryption($params["smtp_encription"]);
        }
        $transport->setPort($params["smtp_port"]);
        if (!empty($params["smtp_localdomain"])) {
            $transport->setLocalDomain($params["smtp_localdomain"]);
        }

        return Swift_Mailer::newInstance($transport);

    }

    /**
     *
     * @param string $protocol
     *
     */
    public function setProtocol($protocol) {

        $this->protocol = $protocol;
    }

    /**
     *
     * @return string
     *
     */
    public function getProtocol() {
        return $this->protocol;
    }

    /**
     *
     * @param string $authority
     *
     */
    public function setAuthority($authority) {
        $this->authority = $authority;
    }

    /**
     *
     * @return string
     *
     */
    public function getAuthority() {
        return $this->authority;
    }

    /**
     *
     * @param string $path
     *
     */
    public function setFinalStatusApi($path) {
        $this->finalStatusApi = $path;
    }

    /**
     *
     * @return string
     *
     */
    public function getFinalStatusApi() {
        return $this->protocol . "://" . $this->authority . $this->finalStatusApi;
    }

    /**
     *
     * @param string $path
     *
     */
    public function setAddMerchantParameterApi($path) {
        $this->addMerchantParameterApi = $path;
    }

    /**
     *
     * @return string
     *
     */
    public function getAddMerchantParameterApi() {
        return $this->protocol . "://" . $this->authority . $this->addMerchantParameterApi;
    }

    /**
     *
     * @param string $path
     *
     */
    public function setRecurrentPaymentApi($path) {
        $this->recurrentPaymentApi = $path;
    }

    /**
     *
     * @return string
     *
     */
    public function getRecurrentPaymentApi() {
        return $this->protocol . "://" . $this->authority . $this->recurrentPaymentApi;
    }

    /**
     *
     * @param string $user
     *
     */
    public function setRecurrentPaymentApiUsername($user) {
        $this->recurrentPaymentApiUsername = $user;
    }

    /**
     *
     * @return string
     *
     */
    public function getRecurrentPaymentApiUsername() {
        return $this->recurrentPaymentApiUsername;
    }

    /**
     *
     * @param string $pass
     *
     */
    public function setRecurrentPaymentApiPassword($pass) {
        $this->recurrentPaymentPassword = $pass;
    }

    /**
     *
     * @return string
     *
     */
    public function getRecurrentPaymentApiPassword() {
        return $this->recurrentPaymentPassword;
    }

    /**
     *
     * @param string $path
     *
     */
    public function setTransactionStatusApi($path) {
        $this->transactionStatusApi = $path;
    }

    /**
     *
     * @return string
     *
     */
    public function getTransactionStatusApi() {
        return $this->protocol . "://" . $this->authority . $this->transactionStatusApi;
    }

    /**
     *
     * @param string $user
     *
     */
    public function setTransactionApiUsername($user) {
        $this->transactionApiUsername = $user;
    }

    /**
     *
     * @return string
     *
     */
    public function getTransactionApiUsername() {
        return $this->transactionApiUsername;
    }

    /**
     *
     * @param string $pass
     *
     */
    public function setTransactionApiPassword($pass) {
        $this->transactionApiPassword = $pass;
    }

    /**
     *
     * @return string
     *
     */
    public function getTransactionApiPassword() {
        return $this->transactionApiPassword;
    }

    /**
     *
     * @param string $merchantConfigId
     *
     */
    public function setMerchantConfigId($merchantConfigId) {
        $this->merchantConfigId = $merchantConfigId;
    }

    /**
     *
     * @return string
     *
     */
    public function getMerchantConfigId() {
        return $this->merchantConfigId;
    }

    /**
     * Cancels a recurrent payment subscription.
     *
     * @return array
     *
     */
    public function cancelRecurrentPaymentSubscription($subscriptionToken) {
        if (!(ini_get('allow_url_fopen'))) {
            $this->doFSocksRCPaymentRequest("POST", $subscriptionToken, "cancel/");
        } else {
            $context = stream_context_create(array(
                'http' => array(
                    'method' => 'POST',
                    'header' => sprintf("Authorization: Basic %s\r\n", base64_encode($this->getRecurrentPaymentApiUsername() . ':' . $this->getRecurrentPaymentApiPassword())) .
                        "Content-type: \r\n",
                    'timeout' => 5,
                ),
            ));

            @file_get_contents($this->getRecurrentPaymentApi() . "cancel/" . $this->merchantConfigId . "/" . $subscriptionToken . "/", false, $context);
        }
        return true;
    }

    /**
     * Sets a stored merchant parameter to a transaction
     *
     * @return array
     *
     */
    public function addMerchantParameter($transactionId, $name, $value) {
        if (!(ini_get('allow_url_fopen'))) {
            $this->doFSocksAddMerchantParameterRequest("POST", $transactionId, $name, $value);
        } else {
            $content = "merchant-config=" . $this->merchantConfigId . "&transaction-id=" . $transactionId .
                "&parameter-name=" . urlencode($name) . "&parameter-value=" . urlencode($value);
            $context = stream_context_create(array(
                'http' => array(
                    'method' => 'POST',
                    'header' => sprintf("Authorization: Basic %s\r\n", base64_encode($this->getTransactionApiUsername() . ':' . $this->getTransactionApiPassword())) .
                        "Content-type: application/x-www-form-urlencoded\r\n" .
                        "Content-length: " . strlen($content) . "\r\n",
                    'content' => $content,
                    'timeout' => 5,
                ),
            ));

            @file_get_contents($this->getAddMerchantParameterApi(), false, $context);
        }
        return "ok";
    }

    private function doFSocksAddMerchantParameterRequest($method, $transactionId, $name, $value) {
        if ($this->protocol == "https://") {
            $port = 443;
            $host = "ssl://" . $this->authority;
        } else {
            $port = 80;
            $host = $this->authority;
        }

        return $this->http_request($method, $host, $port, $this->addMerchantParameterApi, array(), array("merchant-config" => $this->merchantConfigId,
            "transaction-id" => $transactionId, "parameter-name" => urlencode($name), "parameter-value" => urlencode($value)), array(), array("Content-type" => "application/x-www-form-urlencoded", "Authorization" => sprintf("Basic %s", base64_encode($this->getTransactionApiUsername() . ':' . $this->getTransactionApiPassword()))));
    }

    /**
     * Sets final status of a transaction
     *
     * @return array
     *
     */
    public function setFinalStatus($transactionId) {
        if (!(ini_get('allow_url_fopen'))) {
            $this->doFSocksFinalStatusRequest("POST", $transactionId);
        } else {
            $content = "merchant-config=" . $this->merchantConfigId . "&transaction-id=" . $transactionId;
            $context = stream_context_create(array(
                'http' => array(
                    'method' => 'POST',
                    'header' => sprintf("Authorization: Basic %s\r\n", base64_encode($this->getTransactionApiUsername() . ':' . $this->getTransactionApiPassword())) .
                        "Content-type: application/x-www-form-urlencoded\r\n" .
                        "Content-length: " . strlen($content) . "\r\n",
                    'content' => $content,
                    'timeout' => 5,
                ),
            ));

            @file_get_contents($this->getFinalStatusApi(), false, $context);
        }
        return "ok";
    }

    private function doFSocksFinalStatusRequest($method, $transactionId) {
        if ($this->protocol == "https://") {
            $port = 443;
            $host = "ssl://" . $this->authority;
        } else {
            $port = 80;
            $host = $this->authority;
        }

        return $this->http_request($method, $host, $port, $this->finalStatusApi, array(), array("merchant-config" => $this->merchantConfigId,
            "transaction-id" => $transactionId), array(), array("Content-type" => "application/x-www-form-urlencoded", "Authorization" => sprintf("Basic %s", base64_encode($this->getTransactionApiUsername() . ':' . $this->getTransactionApiPassword()))));
    }


    /**
     * Returns information about a recurrent payment subscription
     *
     * @return array
     *
     */
    public function getRecurrentPaymentSubscriptionInfo($subscriptionToken) {
        if (!(ini_get('allow_url_fopen'))) {
            $json = $this->doFSocksRCPaymentRequest("GET", $subscriptionToken, "info/");
        } else {
            $context = stream_context_create(array(
                'http' => array(
                    'method' => 'GET',
                    'header' => sprintf("Authorization: Basic %s\r\n", base64_encode($this->getRecurrentPaymentApiUsername() . ':' . $this->getRecurrentPaymentApiPassword())) .
                        "Content-type: application/json\r\n",
                    'timeout' => 5,
                ),
            ));

            $json = @file_get_contents($this->getRecurrentPaymentApi() . "info/" . $this->merchantConfigId . "/" . $subscriptionToken . "/", false, $context);
        }
        $response = json_decode($json, true);
        if (!is_array($response)) {
            throw new Exception("Could not get retrieve the json response");
        }
        return $response;
    }

    private function doFSocksRCPaymentRequest($method, $token, $pathSegment) {
        if ($this->protocol == "https://") {
            $port = 443;
            $host = "ssl://" . $this->authority;
        } else {
            $port = 80;
            $host = $this->authority;
        }

        return $this->http_request($method, $host, $port, $this->recurrentPaymentApi . $pathSegment . $this->merchantConfigId . "/" . $token . "/", array(), array(), array(), array("Content-type" => "", "Authorization" => sprintf("Basic %s", base64_encode($this->getRecurrentPaymentApiUsername() . ':' . $this->getRecurrentPaymentApiPassword()))));
    }

    public function pollInfoFromEPP($transactionId, $queryParamName) {
        try {
            // check if alllow_url_fopen is enabled, if not do fallback to sockets
            if (!(ini_get("allow_url_fopen"))) {
                $json = $this->doFSocksOpenPollRequest($transactionId, $queryParamName);
            } else {
                $context = stream_context_create(array(
                    'http' => array(
                        'method' => 'GET',
                        'header' => sprintf("Authorization: Basic %s\r\n", base64_encode($this->getTransactionApiUsername() . ':' . $this->getTransactionApiPassword())) .
                            "Content-type: application/x-www-form-urlencoded\r\n",
                        'timeout' => 5,
                    ),
                ));
                $json = @file_get_contents($this->getTransactionStatusApi() . "?merchant-config=" . $this->merchantConfigId . "&" . $queryParamName . "=" . $transactionId, false, $context);
            }

            return $json;
        } catch (Exception $ex) {
            return json_encode(array("error" => "No transaction available."));
        }
    }

    /**
     * Polls the transaction status from epp via merchant transaction id
     *
     * @param string $transactionId
     * @deprecated
     */
    public function pollStatus($transactionId) {
        return $this->pollStatusFromEPP($transactionId, "merchant-trx-id");
    }

    /**
     * Polls the transaction status from epp of a payment transaction
     *
     * @param string $transactionId
     */
    public function pollTransactionStatus($transactionId) {
        return $this->pollStatusFromEPP($transactionId, "transaction-id");
    }

    private function pollStatusFromEPP($transactionId, $queryParamName) {
        try {
            // check if alllow_url_fopen is enabled, if not do fallback to sockets
            if (!(ini_get("allow_url_fopen"))) {
                $json = $this->doFSocksOpenPollRequest($transactionId, $queryParamName);
            } else {
                $context = stream_context_create(array(
                    'http' => array(
                        'method' => 'GET',
                        'header' => sprintf("Authorization: Basic %s\r\n", base64_encode($this->getTransactionApiUsername() . ':' . $this->getTransactionApiPassword())) .
                            "Content-type: application/x-www-form-urlencoded\r\n",
                        'timeout' => 5,
                    ),
                ));

                $json = @file_get_contents($this->getTransactionStatusApi() . "?merchant-config=" . $this->merchantConfigId . "&" . $queryParamName . "=" . $transactionId, false, $context);
            }

            $transaction = json_decode($json);
            if (isset($transaction->status)) {
                foreach ($transaction->status as $status) {
                    if ($status->statusName == "final_success") {
                        $currentStatus = "confirmed";
                    } else {
                        if ($status->statusName == "final_error") {
                            $currentStatus = "declined";
                        } else {
                            if ($status->statusName == "aborted_by_user") {
                                $currentStatus = "declined";
                            } else {
                                $currentStatus = "pending";
                            }
                        }
                    }
                }
                return json_encode(array("status" => $currentStatus));
            } else {
                return json_encode(array("status" => "final_error"));
            }
        } catch (Exception $ex) {
            return json_encode(array("status" => "final_error"));
        }
    }

    private function doFSocksOpenPollRequest($transactionId, $queryParamName) {
        if ($this->protocol == "https://") {
            $port = 443;
            $host = "ssl://" . $this->authority;
        } else {
            $port = 80;
            $host = $this->authority;
        }

        return $this->http_request("GET", $host, $port, $this->transactionStatusApi, array("merchant-config" => $this->getMerchantConfigId(),
            $queryParamName => $transactionId), array(), array(), array("Content-type" => "application/x-www-form-urlencoded", "Authorization" => sprintf("Basic %s", base64_encode($this->getTransactionApiUsername() . ':' . $this->getTransactionApiPassword()))));
    }

    // this method is copied from php.net fsockopen documentation
    private function http_request(
        $verb = 'GET', /* HTTP Request Method (GET and POST supported) */
        $ip, /* Target IP/Hostname */
        $port = 80, /* Target TCP port */
        $uri = '/', /* Target URI */
        $getdata = array(), /* HTTP GET Data ie. array('var1' => 'val1', 'var2' => 'val2') */
        $postdata = array(), /* HTTP POST Data ie. array('var1' => 'val1', 'var2' => 'val2') */
        $cookie = array(), /* HTTP Cookie Data ie. array('var1' => 'val1', 'var2' => 'val2') */
        $custom_headers = array(), /* Custom HTTP headers ie. array('Referer: http://localhost/ */
        $timeout = 10000, /* Socket timeout in milliseconds */
        $req_hdr = false, /* Include HTTP request headers */
        $res_hdr = false /* Include HTTP response headers */
    ) {

        $ret = '';
        $verb = strtoupper($verb);
        $cookie_str = '';
        $getdata_str = count($getdata) ? '?' : '';
        $postdata_str = '';

        foreach ($getdata as $k => $v)
            $getdata_str .= urlencode($k) . '=' . urlencode($v) . '&';

        foreach ($postdata as $k => $v)
            $postdata_str .= urlencode($k) . '=' . urlencode($v) . '&';

        foreach ($cookie as $k => $v)
            $cookie_str .= urlencode($k) . '=' . urlencode($v) . '; ';

        $crlf = "\r\n";
        $req = $verb . ' ' . $uri . $getdata_str . ' HTTP/1.1' . $crlf;
        $req .= 'Host: ' . $ip . $crlf;
        $req .= 'User-Agent: Mozilla/5.0 Firefox/3.6.12' . $crlf;
        $req .= 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' . $crlf;
        $req .= 'Accept-Language: en-us,en;q=0.5' . $crlf;
        $req .= 'Accept-Encoding: deflate' . $crlf;
        $req .= 'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7' . $crlf;

        foreach ($custom_headers as $k => $v)
            $req .= $k . ': ' . $v . $crlf;

        if (!empty($cookie_str)) {
            $req .= 'Cookie: ' . substr($cookie_str, 0, -2) . $crlf;
        }

        if ($verb == 'POST' && !empty($postdata_str)) {
            $postdata_str = substr($postdata_str, 0, -1);
            $req .= 'Content-Type: application/x-www-form-urlencoded' . $crlf;
            $req .= 'Content-Length: ' . strlen($postdata_str) . $crlf . $crlf;
            $req .= $postdata_str;
        } else {
            $req .= $crlf;
        }

        if ($req_hdr) {
            $ret .= $req;
        }

        if (($fp = fsockopen($ip, $port, $errno, $errstr)) == false) {
            return "Error $errno: $errstr\n";
        }

        stream_set_timeout($fp, 0, $timeout * 1000);

        fputs($fp, $req);
        while ($line = fgets($fp))
            $ret .= $line;
        fclose($fp);

        if (!$res_hdr) {
            $ret = substr($ret, strpos($ret, "\r\n\r\n") + 4);
        }

        return $ret;
    }

    /**
     *    Reads the encoded parameters from the cookie
     *
     * @param string $name of the parameter
     * @param string $formId
     */
    public function readFormParameters($name, $formId) {
        $name = $this->decodeParameters($name);
        if (!$this->cookieParameters) {
            $this->cookieParameters = array();
            $tmpCookieParameters = array();
            if (array_key_exists("epik_" . $formId, $_COOKIE)) {
                $tmpCookieParameters = explode("&", $_COOKIE["epik_" . $formId]);
            }
            foreach ($tmpCookieParameters as $paramPairs) {
                $paramPair = explode(":", $paramPairs);
                $this->cookieParameters[$this->decodeParameters($paramPair[0])] = $this->decodeParameters($paramPair[1]);
            }
        }
        if (!array_key_exists($name, $this->cookieParameters)) {
            return "";
        }
        return $this->cookieParameters[$name];
    }

    /**
     *    Reads merchant specific parameters from the cookie
     *
     * @param string $name of the parameter
     * @param string $formId
     */
    public function readFormMerchantParameters($name, $formId) {
        $name = $this->decodeParameters($name);
        if (!$this->cookieMerchantParameters) {
            $this->cookieMerchantParameters = array();
            $tmpCookieMerchantParameters = array();
            if (array_key_exists("epik_" . $formId . "_merchantparameter", $_COOKIE)) {
                $tmpCookieMerchantParameters = explode("&", $_COOKIE["epik_" . $formId . "_merchantparameter"]);
            }
            foreach ($tmpCookieMerchantParameters as $paramPairs) {
                $paramPair = explode(":", $paramPairs);
                $this->cookieMerchantParameters[$this->decodeParameters($paramPair[0])] = $this->decodeParameters($paramPair[1]);
            }
        }
        if (!array_key_exists($name, $this->cookieMerchantParameters)) {
            return "";
        }
        return $this->cookieMerchantParameters[$name];
    }

    public function encodeParameters($inputString) {
        $newString = '';
        $pattern = "/[-A-Za-z0-9]/";
        $characters = preg_split('//u', $inputString, -1, PREG_SPLIT_NO_EMPTY);
        foreach ($characters as $string) {
            if (!preg_match($pattern, $string)) {
                $newString .= "_" . ord(utf8_decode($string)) . "_";
            } else {
                $newString .= $string;
            }
        }
        return $newString;
    }

    public function decodeParameters($string) {
        $newString = $string;

        $pattern = "/_[0-9]+_/";
        $array = array();
        preg_match_all($pattern, $string, $array);
        foreach ($array as $patterns) {
            foreach ($patterns as $val) {
                $char = utf8_encode(chr(str_replace("_", "", $val)));
                $newString = mb_ereg_replace($val, $char, $newString);
            }
        }
        return $newString;
    }

    /**
     * Creates a complete reference number
     *
     * copied from www.sprain.ch, Manuel Reinhard
     *
     * @param string $bankingCustomerIdentification specifies the banking customer identification
     * @param string $code specifies a part of the reference number
     *
     * @return string
     */
    public function createCompleteRefNo($bankingCustomerIdentification, $code) {
        if (strlen($bankingCustomerIdentification) != 6) {
            throw new Exception("BankingCustomerIdentification has to be 6 digits");
        }
        if (!$code) {
            $code = "";
        }
        if (strlen($code) > 20) {
            throw new Exception("Code could not be longer than 20 digits");
        }
        //get reference number and fill with zeros
        $completeReferenceNumber = str_pad($code, 20, '0', STR_PAD_LEFT);

        //add customer identification code
        $completeReferenceNumber = str_pad($bankingCustomerIdentification, 6, '0', STR_PAD_RIGHT) .
            $completeReferenceNumber;

        //add check digit
        $completeReferenceNumber .= $this->modulo10($completeReferenceNumber);

        //return
        return $completeReferenceNumber;
    }

    /**
     * Creates Modulo10 recursive check digit
     *
     * as found on http://www.developers-guide.net/forums/5431,modulo10-rekursiv
     * (thanks, dude!)
     *
     * @param string $number
     * @return int
     */
    private function modulo10($number) {
        $table = array(0, 9, 4, 6, 8, 2, 7, 1, 3, 5);
        $next = 0;
        for ($i = 0; $i < strlen($number); $i++) {
            $next = $table[($next + substr($number, $i, 1)) % 10];
        }//for
        return (10 - $next) % 10;
    }

}

?>
