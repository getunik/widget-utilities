<?php

namespace RaiseNow\WidgetUtilities;

use RaiseNow\WidgetUtilities\YAMLObject;
use RaiseNow\WidgetUtilities\MailDataHelper;

class WidgetParamsUtil
{

    private static $WIDGET_PARAMS = 'widget-parameters.yaml';


    /**
     * returns widget parameters.
     */
    public static function retrieveParams($pathToConfig, $epikParams, $isSecure = true)
    {
        $widgetParamsFilePath = $pathToConfig . self::$WIDGET_PARAMS;
        $secureWidgetParamsFilePath = $pathToConfig . 'secure-' . self::$WIDGET_PARAMS;
        $widgetParams = null;
        if (file_exists($widgetParamsFilePath)) {
            $widgetParams = YAMLObject::FromFile($widgetParamsFilePath);
            if ($isSecure && file_exists($secureWidgetParamsFilePath)) {
                $secureParams = YAMLObject::FromFile($secureWidgetParamsFilePath);
                $widgetParams = new YAMLObject(array_merge($secureParams->getValue(), $widgetParams->getValue()));
            }
        } else {
            $convertedStructure = array();
            $convertedStructure['common'] = array();
            if (isset($epikParams['email'])) {
                $convertedStructure['email'] = $epikParams['email'];
            }
            if (isset($epikParams['amounts'])) {
                $convertedStructure['amounts'] = $epikParams['amounts'];
            }
            if (isset($epikParams['payment_methods'])) {
                $convertedStructure['payment_methods'] = $epikParams['payment_methods'];
            }
            if (isset($epikParams['config'])) {
                foreach ($epikParams['config'] as $key => $val) {
                    switch ($key) {
                        case 'scripts':
                        case 'css':
                            $entries = explode(',', $val);
                            if (count($entries) === 1 && $entries[0] === '') {
                                $convertedStructure['common'][$key] = array();
                            } else {
                                $convertedStructure['common'][$key] = $entries;
                            }
                            break;
                        default:
                            $convertedStructure['common'][$key] = $val;

                    }
                }
            }
            $widgetParams = new YAMLObject($convertedStructure);
        }

        return $widgetParams;
    }

    /**
     * This function fetches an object from the $valueStore according to a simple fallback rule based on the configuration in
     * $widgetParams. Lets take the following YAML snippet as example, ($fallbackName = 'some_fallback')
     *   some_fallback:
     *     base: base.key.name
     *     hierarchy: ['some_param', 'some_other_param']
     * Assuming $parameterSource = array('some_param' => 'x', 'some_other_param' => 'y'), the function will search the following
     * keys in order until it finds one that exists:
     *   - base.key.name_x_y
     *   - base.key.name_x
     *   - base.key.name
     *
     * @param $widgetParams YAMLObject - the widget parameters configuration object (@see retrieveParams)
     * @param $valueSource YAMLObject - the YAML object in which the parametrized fallback key is searched (usually translations YAML)
     * @param $fallbackName string - the name of the fallback configuration in $widgetParams (one of the keys defined under 'fallback_keys')
     * @param $parameterSource array - the parameter value dictionary that is used to generate the actuall fallback key segments
     * @return YAMLObject the first value matching the fallback key rule or NULL if no match was found
     */
    public static function getFallbackKeyValue(YAMLObject $widgetParams, YAMLObject $valueSource, $fallbackName, $parameterSource)
    {
        if (isset($widgetParams['fallback_keys.' . $fallbackName])) {
            $keyDefinition = $widgetParams['fallback_keys.' . $fallbackName];

            $hierarchyValues = array($keyDefinition['base']);
            foreach ($keyDefinition['hierarchy'] as $paramName) {
                $hierarchyValues[] = empty($parameterSource[$paramName]) ? 'null' : $parameterSource[$paramName];
            }

            while (count($hierarchyValues) > 0) {
                $tmpKey = implode('_', $hierarchyValues);
                if (isset($valueSource[$tmpKey])) {
                    return $valueSource[$tmpKey];
                }
                $hierarchyValues = array_slice($hierarchyValues, 0, -1);
            }
        }

        return NULL;
    }
}
