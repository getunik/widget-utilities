<?php

namespace RaiseNow\WidgetUtilities;

use DateTime;
use RaiseNow\WidgetUtilities\CronHelper;


class MailDataHelper {

    public static function BuildMailDataArray(YAMLObject $trans, $language) {
        $mailData = array();

        // merge general mail parameters from translation to mail data
        $trans['email.general.parameters']->mergeInto($mailData);
        // merge all request parameters into mail data
        $mailData = array_merge($mailData, $_REQUEST);

        $mailData['date'] = self::getCurrentDate();
        $mailData['amount'] = strtoupper(self::getRequest('currency', 'CHF')) . ' ' . self::getRequest('amount');
        $mailData['interval'] = self::getInterval($trans);
        $mailData['payment_method'] = self::getPaymentMethod($trans);
        $mailData['masked_cc'] = self::getRequest('masked_cc');
        $mailData['transaction_id'] = self::getRequest('epp_transaction_id');
        $mailData['comments'] = self::getRequest('stored_customer_message');
        $mailData['subscription_edit_url'] = self::getSubscriptionEditUrl($trans, $language);
        $mailData['charges_count'] = self::getRequest('charges_count');
        $mailData['download_url'] = self::getDownloadUrl();
        $mailData['email_permission'] = self::getEmailPermission($trans, 'customer');

        $mailData['salutation'] = $trans['email.general.salutation.' . self::getRequest('stored_customer_salutation')];
        $mailData['salutation_long'] = $trans['email.general.salutation_long.' . self::getRequest('stored_customer_salutation')];
        $mailData['firstname'] = self::getRequest('stored_customer_firstname');
        $mailData['lastname'] = self::getRequest('stored_customer_lastname');
        $mailData['address'] = self::getAddress($trans, 'customer');
        $mailData['country'] = self::getCountry($trans, 'customer');

        $mailData['recipient_salutation'] = $trans['email.general.salutation.' . self::getRequest('stored_recipient_salutation')];
        $mailData['recipient_salutation_long'] = $trans['email.general.salutation_long.' . self::getRequest('stored_recipient_salutation')];
        $mailData['recipient_firstname'] = self::getRequest('stored_recipient_firstname');
        $mailData['recipient_lastname'] = self::getRequest('stored_recipient_lastname');
        $mailData['recipient_address'] = self::getAddress($trans, 'recipient');
        $mailData['recipient_country'] = self::getCountry($trans, 'recipient');

        return $mailData;
    }

    private static function getRequest($key, $default=null) {
        return isset($_REQUEST[$key]) ? $_REQUEST[$key] : $default;
    }

    private static function isRequestEmpty($key) {
        return !isset($_REQUEST[$key]) || empty($_REQUEST[$key]);
    }

    private static function getCurrentDate() {
        $now = new DateTime();
        return $now->format("Y-m-d H:i:s");
    }

    private static function getInterval(YAMLObject $trans) {
        $intervalName = CronHelper::expressionToInterval(self::getRequest('interval', ''));
        $intervalKey = "email.general.recurring_intervals.{$intervalName}";

        if ($trans->offsetExists($intervalKey)) {
            $interval = $trans[$intervalKey];
        } else {
            // fall back to legacy interval expression translation
            $intervalBaseKey = 'subscription.intervals.interval_';

            // start out with default text
            $interval = $trans[$intervalBaseKey . 'default'];

            if (isset($_REQUEST['interval'])) {
                $matches = array();
                preg_match('/([^ ]+ [^ ]+ [^ ]+)$/', self::getRequest('interval'), $matches);
                $intervalKey = preg_replace('/ /', '_', $matches[1]);
                if ($trans->offsetExists($intervalBaseKey . $intervalKey)) {
                    $interval = $trans[$intervalBaseKey . $intervalKey];
                }
            }
        }

        return $interval;
    }

    private static function getPaymentMethod(YAMLObject $trans) {
        return $trans['common.payment_methods.' . strtolower(self::getRequest('payment_method'))];
    }

    private static function getAddress(YAMLObject $trans, $source) {
        $lines = array();
        if (!self::isRequestEmpty("stored_{$source}_street"))
        {
            $lines[] = self::getRequest("stored_{$source}_street") . " " . self::getRequest("stored_{$source}_street_number");
        }
        if (!self::isRequestEmpty("stored_{$source}_street2"))
        {
            $lines[] = self::getRequest("stored_{$source}_street2");
        }
        if (!self::isRequestEmpty("stored_{$source}_zip_code") || !self::isRequestEmpty("stored_{$source}_city"))
        {
            $lines[] = self::getRequest("stored_{$source}_zip_code") . " " . self::getRequest("stored_{$source}_city");
        }
        if (!self::isRequestEmpty("stored_{$source}_country"))
        {
            $lines[] = self::getCountry($trans, $source);
        }

        return implode("\n", array_filter(array_map(function ($l) { return trim($l); }, $lines), function ($l) { return !empty($l); }));
    }

    private static function getSubscriptionEditUrl(YAMLObject $trans, $language) {
        $url = $trans->get('common.subscription_edit_url', true);
        if(!$url) {
            $url = self::getRequest('stored_rnw_source_url');
        }
        $urlParts = parse_url($url);
        $queryString = '';
        if ($urlParts && isset($urlParts['query'])) {
            $query = array();
            // merge query parameters from url and target
            parse_str($urlParts['query'], $query);
            if (array_key_exists('dds-subscription', $query)) {
                unset($query['dds-subscription']);
            }
            if (array_key_exists('action', $query)) {
                unset($query['action']);
            }
            // this will use application/x-www-form-urlencoded (wrongly)
            $queryString .= http_build_query($query);
        }
        // strip hash string from original url
        if (false !== $pos = strpos($url, '#')) {
            $url = substr($url, 0, $pos);
        }

        // strip query string from original url
        if (false !== $pos = strpos($url, '?')) {
            $url = substr($url, 0, $pos);
        }
        $url .= '?';
        if (strlen($queryString) > 0) {
            $url .= $queryString . '&';
        }
        return $url . 'action=info&dds-subscription=' . self::getRequest('subscription_token');
    }

    /**
     * Unfortunately, there is some major inconsistency in parameter naming. Currently, there are
     * the two versions 'xypayment_pdflink' (es,ezs) and 'xy_pdflink' (dd)
     *
     * @return string the download URL for the current transaction or NULL if there is none
     */
    private static function getDownloadUrl() {
        $url = self::getRequest(self::getRequest('payment_method') . 'payment_pdflink');
        if ($url === NULL) {
            $url = self::getRequest(self::getRequest('payment_method') . '_pdflink');
            if ($url === NULL) {
                $url = self::getRequest('pdflink');
            }
        }

        return $url;
    }

    private static function getEmailPermission(YAMLObject $trans, $source) {
        $permission = self::getRequest("stored_{$source}_email_permission") === NULL ? 'no' : 'yes';
        return $trans['email.general.boolean'][$permission];
    }

    private static function getCountry(YAMLObject $trans, $source) {
        $country = self::getRequest("stored_{$source}_country", 'CH');
        return $trans['countries.' . $country];
    }
}
