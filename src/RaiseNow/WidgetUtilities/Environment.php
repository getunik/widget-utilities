<?php

namespace RaiseNow\WidgetUtilities;

class Environment {
    public $language;
	public $currency;
    public $widgetUUID;
    public $country;

    function __construct($lang='de', $currency='chf', $widgetUUID='1234567890', $country='ch') {
        $this->language = $this->getLanguage()? $this->getLanguage() : $lang;
        $this->currency = $this->getCurrency()? $this->getCurrency() : $currency;
        $this->widgetUUID = $this->getWidgetUUID()? $this->getWidgetUUID() : $widgetUUID;
        $this->country = $country;
    }

    protected function getCountry() {
        return $this->country;
    }

    protected function getLanguage() {
        if (isset($_REQUEST['language'])) {
            $this->language = $_REQUEST['language'];
        } else if (isset($_REQUEST['lang'])) {
            $this->language = $_REQUEST['lang'];
        }
        return $this->language;
    }

	protected function getCurrency() {
		if (isset($_REQUEST['currency'])) {
            $this->currency = $_REQUEST['currency'];
        }
        return $this->currency;
	}

    protected function getWidgetUUID() {
        if (isset($_REQUEST['widget-uuid'])) {
            $this->widgetUUID = $_REQUEST['widget-uuid'];
        }
        return $this->widgetUUID;
    }
}
