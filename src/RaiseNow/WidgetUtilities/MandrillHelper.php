<?php

namespace RaiseNow\WidgetUtilities;

class MandrillHelper {

    private static $MANDRILL_API_URL = "https://mandrillapp.com/api/1.0/messages/send-template.json";


    /**
     * Sends data to mandrill via http widget parameters.
     *
     * @param $apiKey mandrill api key
     * @param $templateName mandrill identifier for the email template
     * @param array $message
     * @param array $templateContent
     * @return mixed
     */
    public static function sendJson($apiKey, $templateName, array $message, array $templateContent = array()) {

        $data = array(
            "key" => $apiKey,
            "template_name" => $templateName,
            "template_content" => $templateContent,
            "message" => $message,
            "async"=> false,
            "ip_pool"=> "Main Pool"
        );

        $ch = curl_init(self::$MANDRILL_API_URL);
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Accept: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($data)
        ));
        // Send the request
        $response = curl_exec($ch);
        // Check for errors
        if($response === FALSE){
            die(curl_error($ch));
        }
        // Decode the response
        $responseData = json_decode($response, TRUE);
        return $responseData;

    }

    /**
     * create global_merge_vars (https://mandrill.zendesk.com/hc/en-us/articles/205582487-How-do-I-use-merge-tags-to-add-dynamic-content)
     * from key value array (e.g. $_REQUEST)
     *
     * @param array $data key value pairs which will be added to the global_merge_vars
     * @return array
     */
    public static function createGlobalMergeVars(array $data) {
        $globalMergeVars = array();
        foreach($data as $key => $val) {
            $globalMergeVars[] = array("name" => $key, "content" => $val);
        }
        return $globalMergeVars;
    }

    /**
     * Creates message consists of key value pairs which will be sent to mandrill via sendJson
     *
     * @param $subject
     * @param $senderAddress
     * @param $senderName
     * @param array $recipients array of key value pairs containing name, email and type (cc or bcc) as keys
     * @param array $globalMergeVars
     * @return array
     */
    public static function createMessage($subject, $senderAddress, $senderName, array $recipients, array $globalMergeVars, array $mergeVars = array()) {

        $message = array(
            "subject" => $subject,
            "from_email" => $senderAddress,
            "to" => $recipients,
            "merge" => true,
            "merge_language" => "mailchimp",
            "global_merge_vars" => $globalMergeVars,
        );
        if($senderName) {
            $message["from_name"] = $senderName;
        }
        if(count($mergeVars) > 0) {
            $message["merge_vars"] = $mergeVars;
        }
        return $message;
    }
}
