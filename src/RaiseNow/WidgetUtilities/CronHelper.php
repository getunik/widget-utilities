<?php

namespace RaiseNow\WidgetUtilities;

use DateTime;

/**
 * Helps translate simple interval terms into cron expressions and back
 */
class CronHelper {

    const INTERVAL_YEARLY = 'yearly';
    const INTERVAL_SEMESTRAL = 'semestral';
    const INTERVAL_QUARTERLY = 'quarterly';
    const INTERVAL_MONTHLY = 'monthly';
    const INTERVAL_WEEKLY = 'weekly';
    const INTERVAL_UNKNOWN = 'unknown';

    const MAPPING_MODE_LAST_OF = 'last-of';
    const MAPPING_MODE_FIRST_OF = 'first-of';
    const MAPPING_MODE_JIT = 'jit';

    /**
     * Helper method that generates a comma separated list of months starting with $start and
     * a step size of $step. It will always return the months for a full year, but no more.
     *
     * @param $start start month [1, 12]
     * @param $step interval month step [1, 12]
     * @return string
     */
    public static function getMonthList($start, $step) {
        $offset = $start - 1;
        $months = array();
        for ($i = 0; $i < 12; $i += $step) {
            $months[] = (($offset + $i) % 12) + 1;
        }
        return implode(',', $months);
    }

    /**
     * Converts the given interval name to a cron expression using the mapping mode to define
     * how exactly the resulting cron expression is constructed.
     *
     * @param $interval one of the CronHelper::INTERVAL_* constants
     * @param $mode (optional) one of the CronHelper::MAPPING_MODE_* constants
     * @return string a cron expression
     */
    public static function intervalToExpression($interval, $mode = self::MAPPING_MODE_LAST_OF) {
        if ($mode === self::MAPPING_MODE_LAST_OF) {
            return self::intervalToExpressionLastOf($interval);
        } else if ($mode === self::MAPPING_MODE_FIRST_OF) {
            return self::intervalToExpressionFirstOf($interval);
        } else if ($mode === self::MAPPING_MODE_JIT) {
            return self::intervalToExpressionJit($interval);
        }
    }

    /**
     * Converts the given interval name to a cron expression with the first match being the last
     * day of the month, quarter, semester, year - or the next Friday if set to weekly.
     *
     * @param $interval one of the CronHelper::INTERVAL_* constants
     * @return string
     */
    private static function intervalToExpressionLastOf($interval) {
        switch ($interval) {
            case self::INTERVAL_WEEKLY:
                return "* * 5"; // Friday
            case self::INTERVAL_MONTHLY:
                return "L * *";
            case self::INTERVAL_QUARTERLY:
                return "L */3 *";
            case self::INTERVAL_SEMESTRAL:
                return "L */6 *";
            case self::INTERVAL_YEARLY:
                return "L 12 *";
            default:
                return $interval;
        }
    }

    /**
     * Converts the given interval name to a cron expression with the first match being the first
     * day of the month, quarter, semester, year - or the next Monday if set to weekly.
     *
     * @param $interval one of the CronHelper::INTERVAL_* constants
     * @return string
     */
    private static function intervalToExpressionFirstOf($interval) {
        switch ($interval) {
            case self::INTERVAL_WEEKLY:
                return "* * 1"; // Monday
            case self::INTERVAL_MONTHLY:
                return "1 * *";
            case self::INTERVAL_QUARTERLY:
                return "1 1/3 *";
            case self::INTERVAL_SEMESTRAL:
                return "1 1/6 *";
            case self::INTERVAL_YEARLY:
                return "1 1 *";
            default:
                return $interval;
        }
    }

    /**
     * Converts the given interval name to a cron expression with the first match being 'today'
     * and all subsequent matches being on the same day of the month (days 29, 30, 31 are always
     * converted to the last day of the month by the RNW API). For weekly intervals, the same
     * day-of-week is matched every week. If any unrecognized string or the
     * CronHelper::INTERVAL_UNKNOWN constant are passed, the $interval string is returned as-is.
     *
     * @param $interval one of the CronHelper::INTERVAL_* constants
     * @return string
     */
    private static function intervalToExpressionJit($interval) {
        $today = new DateTime();
        $day = $today->format('j');
        $month = $today->format('n');
        $dow = $today->format('w'); // Su => 0, Mo => 1, ...

        switch ($interval) {
            case self::INTERVAL_WEEKLY:
                return "* * $dow";
            case self::INTERVAL_MONTHLY:
                return "$day * *";
            case self::INTERVAL_QUARTERLY:
                $months = self::getMonthList($month, 3);
                return "$day $months *";
            case self::INTERVAL_SEMESTRAL:
                $months = self::getMonthList($month, 6);
                return "$day $months *";
            case self::INTERVAL_YEARLY:
                return "$day $month *";
            default:
                return $interval;
        }
    }

    /**
     * This is by no means a thorough mapping - it really only works with expressions of a similar form
     * than the ones generated by the intervallToExpression function. If a 5-part expression is given,
     * the minutes and hours parts of it are simply ignored.
     *
     * @param $expression a 3 or 5-part cron expression as used by the RNW API (minutes hours day-of-month month day-of-week)
     * @return string one of the CronHelper::INTERVAL_* constants
     */
    public static function expressionToInterval($expression) {
        if (preg_match('/[^\s]+\s+[^\s]+\s+([^\s]+\s+[^\s]+\s+[^\s]+)/', $expression, $matches)) {
            $expression = $matches[1];
        }

        return self::parseExpression($expression);
    }

    /**
     * Actual implementation of the fairly basic cron expression mapping.
     *
     * @param $expression a 3-part cron expression as used by the RNW API (day-of-month month day-of-week)
     * @return string one of the CronHelper::INTERVAL_* constants
     */
    private static function parseExpression($expression) {
        $matches = null;
        $monthMatches = null;

        // matches strings of the form "xxx yyy group1 group2 group3" where group1
        // is the day(s), group2 the month(s) and group3 the day of week of the cron expression
        if (preg_match('/([^\s]+)\s+([^\s]+)\s+([^\s]+)/', $expression, $matches)) {
            // expressions with multiple day-of-month values are not supported => the day-of-month
            // group has to be an integer, 'L' or '*'
            if (preg_match('/^\d+|L|\*$/', $matches[1])) {
                // if the day-of-week is an integer, then we assume a weekly pattern
                if (preg_match('/^\d+$/', $matches[3])) {
                    return self::INTERVAL_WEEKLY;
                // if any day-of-month is matched, we assume a monthly pattern
                } else if ($matches[2] === '*') {
                    return self::INTERVAL_MONTHLY;
                } else {
                    // if the month is an integer, we assume a yearly interval
                    if (preg_match('/^\d+$/', $matches[2])) {
                        return self::INTERVAL_YEARLY;
                    // if the month has the form X/N where X is '*' or an integer and N is an integer,
                    // then we have a simple quarterly or semestral pattern
                    } else if (preg_match('/^(?:\*|\d+)\/(\d+)$/', $matches[2], $monthMatches)) {
                        if ($monthMatches[1] == 3)
                            return self::INTERVAL_QUARTERLY;
                        if ($monthMatches[1] == 6)
                            return self::INTERVAL_SEMESTRAL;
                    } else {
                        // if the month is a comma separated list, then we assume the number of
                        // entries tells us what we need to know
                        $len = count(explode(',', $matches[2]));
                        if ($len == 4)
                            return self::INTERVAL_QUARTERLY;
                        if ($len == 2)
                            return self::INTERVAL_SEMESTRAL;
                    }
                }
            }
        }

        return self::INTERVAL_UNKNOWN;
    }

}
