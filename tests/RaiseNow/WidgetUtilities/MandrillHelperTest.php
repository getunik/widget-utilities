<?php

namespace Tests\RaiseNow\WidgetUtilities;

use PHPUnit_Framework_TestCase;
use RaiseNow\WidgetUtilities\MandrillHelper;

class MandrillHelperTest extends PHPUnit_Framework_TestCase {

    public function test_create_global_merge_vars() {
        $globalMergeVars = MandrillHelper::createGlobalMergeVars(['stored_my_parameter' => 'myvalue', 'stored_my_parameter2' => 'myvalue2']);
        $this->assertEquals($globalMergeVars[0]['name'], 'stored_my_parameter');
        $this->assertEquals($globalMergeVars[0]['content'], 'myvalue');
        $this->assertEquals($globalMergeVars[1]['name'], 'stored_my_parameter2');
        $this->assertEquals($globalMergeVars[1]['content'], 'myvalue2');
        return $globalMergeVars;
    }


    /**
     * @depends test_create_global_merge_vars
     */
    public function test_create_message(array $globalMergeVars) {

        $message = MandrillHelper::createMessage('mysubject', 'myemailaddress', 'myname', [['name' => 'myname', 'email' => 'myemail@test.ch']], $globalMergeVars);
        $this->assertEquals($message['subject'], 'mysubject');
        $this->assertEquals($message['from_email'], 'myemailaddress');
        $this->assertEquals($message['from_name'], 'myname');
        $this->assertEquals($message['to'][0]['name'], 'myname');
        $this->assertEquals($message['to'][0]['email'], 'myemail@test.ch');
        $this->assertEquals($message['global_merge_vars'][0]['name'], 'stored_my_parameter');
        $this->assertEquals($message['global_merge_vars'][0]['content'], 'myvalue');
    }
}