<?php

namespace Tests\RaiseNow\WidgetUtilities;

use PHPUnit_Framework_TestCase;
use RaiseNow\WidgetUtilities\CronHelper;
use DateTime;

class CronHelperTest extends PHPUnit_Framework_TestCase {

    public function test_get_month_list() {
        //monthly
        $list= CronHelper::getMonthList(1,1);
        $this->assertEquals('1,2,3,4,5,6,7,8,9,10,11,12', $list);

        //quarterly
        $list= CronHelper::getMonthList(1,3);
        $this->assertEquals('1,4,7,10', $list);

        //semestral
        $list= CronHelper::getMonthList(1,6);
        $this->assertEquals('1,7', $list);

        //semestral
        $list= CronHelper::getMonthList(1,12);
        $this->assertEquals('1', $list);

        //monthly
        $list= CronHelper::getMonthList(2,1);
        $this->assertEquals('2,3,4,5,6,7,8,9,10,11,12,1', $list);
        return $list;
    }

    /**
     * @depends test_get_month_list
     */
    public function test_interval_to_expression($list) {
        //quarterly
        $expression= CronHelper::intervalToExpression(CronHelper::INTERVAL_QUARTERLY, CronHelper::MAPPING_MODE_LAST_OF);
        $this->assertEquals('L */3 *', $expression);
        $expression= CronHelper::intervalToExpression(CronHelper::INTERVAL_QUARTERLY, CronHelper::MAPPING_MODE_FIRST_OF);
        $this->assertEquals('1 1/3 *', $expression);

        //semestral
        $expression= CronHelper::intervalToExpression(CronHelper::INTERVAL_SEMESTRAL, CronHelper::MAPPING_MODE_LAST_OF);
        $this->assertEquals( 'L */6 *', $expression);
        $expression= CronHelper::intervalToExpression(CronHelper::INTERVAL_SEMESTRAL, CronHelper::MAPPING_MODE_FIRST_OF);
        $this->assertEquals('1 1/6 *', $expression);


        //yearly
        $expression= CronHelper::intervalToExpression(CronHelper::INTERVAL_YEARLY, CronHelper::MAPPING_MODE_LAST_OF);
        $this->assertEquals('L 12 *', $expression);
        $expression= CronHelper::intervalToExpression(CronHelper::INTERVAL_YEARLY, CronHelper::MAPPING_MODE_FIRST_OF);
        $this->assertEquals('1 1 *', $expression);

        //monthly
        $expression= CronHelper::intervalToExpression(CronHelper::INTERVAL_MONTHLY, CronHelper::MAPPING_MODE_LAST_OF);
        $this->assertEquals('L * *', $expression);
        $expression= CronHelper::intervalToExpression(CronHelper::INTERVAL_MONTHLY, CronHelper::MAPPING_MODE_FIRST_OF);
        $this->assertEquals('1 * *', $expression);
        $expression= CronHelper::intervalToExpression(CronHelper::INTERVAL_MONTHLY, CronHelper::MAPPING_MODE_JIT);
        $now = new DateTime("now");
        $this->assertEquals($now->format('j') . ' * *', $expression);
        return $expression;
    }

    /**
     * @depends test_interval_to_expression
     */
    public function test_expression_to_interval($expression) {
        $interval = CronHelper::expressionToInterval($expression);
        $this->assertEquals('monthly', $interval);

        $interval = CronHelper::expressionToInterval('11 12 *');
        $this->assertEquals('yearly', $interval);

        $interval = CronHelper::expressionToInterval('15 3,6,9,12 *');
        $this->assertEquals('quarterly', $interval);

        $interval = CronHelper::expressionToInterval('1 1,4,7,10 *');
        $this->assertEquals('quarterly', $interval);

        $interval = CronHelper::expressionToInterval('1 2,9 *');
        $this->assertEquals('semestral', $interval);

    }

}